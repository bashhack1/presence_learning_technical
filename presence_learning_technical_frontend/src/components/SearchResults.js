import styled from 'styled-components';
import { withRouter, Link } from 'react-router-dom';
import { GoPencil as PencilIcon } from 'react-icons/go';
import { BiTime as ClockIcon } from 'react-icons/bi';

const Results = styled.div`
  display: flex;
  flex-direction: column;
`;

const List = styled.div``;

const StyledLink = styled(Link)`
  font-size: 1rem;
  font-weight: 700;
  text-decoration: none;
  color: #0d4436;

  &:hover {
    text-decoration: underline;
  }
`;
const Issue = styled.div`
  padding: 1rem 0 1rem 0;
  border-bottom: 1px solid black;
`;

const Title = styled.div`
  font-size: 0.9rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
const Author = styled.div`
  font-size: 0.7rem;
`;
const CreatedAt = styled.div``;
const UpdatedAt = styled.div``;
const Details = styled.div`
  font-size: 0.7rem;
  display: flex;
  gap: 1rem;
  line-height: 1.25rem;
`;

// TODO/ITERATIVE IMPROVEMENTS:


function SearchResults({ data, owner, repo }) {
  return (
    <Results>
      <List>
        {(data?.issues || []).map(
          (r) => {
            const {
              number,
              title,
              user,
              updated_at,
              created_at
            } = r;
            let createdAt = new Date(created_at).toDateString();
            let updatedAt = new Date(updated_at).toDateString();
            return (
              <Issue key={number}>
                <StyledLink
                  to={{
                    pathname: `/issue/${number}`,
                    state: { data: r, updatedAt: updatedAt, createdAt: createdAt }
                  }}
                >
                  {title && <Title>{r.title}</Title>}
                </StyledLink>
                <Details>
                  <Author>
                    <PencilIcon />
                    Issue Author: {user.login}
                  </Author>
                  <CreatedAt>
                    <ClockIcon />
                    {createdAt ? `Created: ${createdAt}` : ''}
                  </CreatedAt>
                  <UpdatedAt>
                    <ClockIcon />
                    {updatedAt ? `Updated: ${updatedAt}` : ''}
                  </UpdatedAt>
                </Details>
              </Issue>
            );
          }
        )}
      </List>
    </Results>
  );
}

export default withRouter(SearchResults);
