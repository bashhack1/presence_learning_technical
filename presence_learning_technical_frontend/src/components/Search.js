import { useEffect, useState, useCallback } from 'react';
import { useParams, Link, withRouter, Redirect } from 'react-router-dom';
import { css } from 'styled-components';
import { Spinner } from 'react-spinners-css';
import { FaGithub } from 'react-icons/fa';
import styled from 'styled-components';
import { DivFlexCenter } from '../globals/styles';
import SearchResults from './SearchResults';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: row;
    align-items: left;
    justify-content: space-between;
    background-color: ${theme.colors.secondary};
    padding: 1rem;
    color: ${theme.colors.primary};
  `}
`;

const IconLink = styled(Link)`
  ${({ theme }) => css`
    text-decoration: none;
    display: flex;
    align-items: center;
    font-size: 1.5rem;
    color: ${theme.colors.primary};
  `}
`;

const ResultsWrapper = styled.div`
  display: flex;
  gap: 2rem;
  padding: 1rem;
  font-size: 0.8rem;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
  width: 75%;

  @media screen and (max-width: 800px) {
    width: 100%;
  }
`;

const Loading = styled(DivFlexCenter)`
  height: 50vh;
  top: 25%;
  left: 50%;
`;

function Search() {
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(false);

  let { owner, repo } = useParams();

  let input = `{ "owner": "${owner}", "repo": "${repo}" }`;

  const fetchData = useCallback(
    (input) => {
      setLoading(true);

      let inputAsObject = JSON.parse(input);

      let { owner, repo } = inputAsObject;

      let url = `http://localhost:8000/api/v1/issues/?owner=${owner}&repo=${repo}`;
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          setData({
            issueCount: data.length,
            issues: data
          });
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
          setError(true);
        });

    },
    [page, input]
  );

  useEffect(() => {
    fetchData(input);
  }, [input, page, fetchData]);

  return (
    <Container id='Search'>
      <Header>
        <IconLink to={`/`}>
          <FaGithub />
        </IconLink>
      </Header>
      <ResultsWrapper>
        <List>
          {loading ? (
            <Loading>
              <Spinner color={`#cf9fff`} />
            </Loading>
          ) : data ? (
            <>
              <SearchResults
                fetchData={fetchData}
                data={data}
                owner={owner}
                repo={repo}
              />
            </>
          ) : null}
        </List>
      </ResultsWrapper>
      {error ? <Redirect to='/error' /> : null}
    </Container>
  );
}

export default withRouter(Search);
