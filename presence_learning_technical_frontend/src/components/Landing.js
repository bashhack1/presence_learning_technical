import { useState } from 'react';
import { useForm } from 'react-hook-form';
import styled, { css } from 'styled-components';
import { GoMarkGithub as GithubIcon } from 'react-icons/go';
import { useHistory, withRouter } from 'react-router-dom';
import { DivFlexCenter } from '../globals/styles';
import { FaSearch } from 'react-icons/fa';
import { MdClear } from 'react-icons/md';

const Container = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.secondary};
    color: ${theme.colors.primary}
  `}
`;

const Wrapper = styled(DivFlexCenter)`
  height: 100vh;
  flex-direction: column;
  gap: 1rem;
`;

const SearchInputs = styled.div`
  ${({ theme }) => css`
    display: flex;
    border: 2px solid ${theme.colors.primary};
    border-radius: 10px;
    padding: 0.25rem;
  `}
`;

const SearchInput = styled.input`
  ${({ theme }) => css`
    border: 0;
    font-size: 1rem;
    outline: none;
    background-color: transparent;
    text-overflow: ellipsis;
    max-width: 35rem;
    color: ${theme.colors.primary};

    ::placeholder {
      color: ${theme.colors.primary};
    }
  `}
`;

const Icon = styled.div`
  ${({ theme }) => css`
    height: 2rem;
    width: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;

    &:hover {
      color: ${theme.colors.tertiary};
    }
  `}
\`;
`;

const Title = styled(DivFlexCenter)`
  ${({ theme }) => css`
    font-size: ${theme.fontSizes.title};
    gap: 1rem;
  `}
`;

const Buttons = styled(DivFlexCenter)``;

const BackButton = styled.button`
  ${({ theme }) => css`
    font-size: 1rem;
    width: 10rem;
    border: 2px solid ${theme.colors.primary};
    border-radius: 10px;
    padding: 0.25rem;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0.5rem;
    color: ${theme.colors.secondary};

    &:hover {
      background-color: ${theme.colors.tertiary};
    }
  `}
`;

function Landing() {
  const history = useHistory();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [ownerInput, setOwnerInput] = useState(null);
  const [repoInput, setRepoInput] = useState(null);

  const routeChange = (input) => {
    const { owner, repo } = input;
    let path = `/search/${owner}/${repo}`;
    history.push(path);
  };

  const handleOwnerInput = (e) => {
    setOwnerInput(e.target.value);
  };
  const handleRepoInput = (e) => {
    setRepoInput(e.target.value);
  };

  return (
    <Container id='Search'>
      <Wrapper>
        <Title>
          <GithubIcon />GitHub Issues Search App
        </Title>
        <form onSubmit={handleSubmit(routeChange)}>
          <label>Owner Name</label>
          <SearchInputs>
            <SearchInput type='text' {...register('owner', { required: true })} />
            <Icon>{ownerInput && <MdClear value='' onClick={handleOwnerInput} />}</Icon>
          </SearchInputs>
          {errors.owner && <p style={{ color: 'tomato' }}>Owner name is required</p>}

          <p />

          <label>Repository Name</label>
          <SearchInputs>
            <SearchInput type='text' {...register('repo', { required: true })} />
            <Icon>{repoInput && <MdClear value='' onClick={handleRepoInput} />}</Icon>
          </SearchInputs>
          {errors.repo && <p style={{ color: 'tomato' }}>Repository name is required</p>}

          <p />
          <Buttons>
            <BackButton>
              <FaSearch />
              Find Issues
            </BackButton>
          </Buttons>

        </form>
      </Wrapper>
    </Container>
  );
}

export default withRouter(Landing);
