import styled, { css } from 'styled-components';
import { DivFlexCenter } from '../globals/styles';
import { AiOutlineNumber as NumberIcon } from 'react-icons/ai';
import { BiArrowBack } from 'react-icons/bi';
import {
  GoPerson as UserIcon,
  GoCommentDiscussion as CommentsIcon,
  GoWatch as UpdatedIcon,
  GoWatch as CreatedIcon,
  GoLock as StatusIcon
} from 'react-icons/go';
import {
  TfiWrite as IssuesIcon
} from 'react-icons/tfi';

import {
  useLocation,
  useHistory,
  withRouter,
  Redirect
} from 'react-router-dom';

const Container = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    height: 100%;
    padding: 0;
    margin: 0;
  `}
`;

const Header = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: row;
    align-items: left;
    justify-content: space-between;
    background-color: ${theme.colors.secondary};
    padding: 1rem;
    color: ${theme.colors.primary};
  `}
`;

const Section = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: left;
  padding: 2rem;
  margin: auto;
  gap: 1rem;
  width: 75%;
`;

const NameLink = styled.a`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 1rem;
  font-size: 1.25rem;
  text-decoration: none;
  color: black;

  &:hover {
    text-decoration: underline;
  }
`;

const Detail = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 1rem;
  font-size: 1rem;
`;

const DetailLink = styled.a`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  gap: 1rem;
  text-decoration: none;
  color: black;

  &:hover {
    text-decoration: underline;
  }
`;

const Buttons = styled(DivFlexCenter)``;

const BackButton = styled.div`
  ${({ theme }) => css`
    width: 10rem;
    border: 2px solid ${theme.colors.secondary};
    border-radius: 10px;
    padding: 0.25rem;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0.5rem;
    color: ${theme.colors.secondary};

    &:hover {
      background-color: ${theme.colors.tertiary};
    }
  `}
`;

function IssueDetail() {
  const location = useLocation();
  const history = useHistory();
  if (!location.state) {
    return <Redirect to='/error' />;
  }
  const { data, updatedAt, createdAt } = location.state;
  const {
    title,
    html_url,
    state,
    user,
    body,
    comments,
    number
  } = data;

  return (
    <Container>
      <Header> Repository Details</Header>
      <Section>
        <NameLink href={html_url} target='_blank' rel='noopener noreferrer'>
          Title: {title}
        </NameLink>
        <Detail>
          <NumberIcon />
          Issue ID: <p>{number}</p>
        </Detail>
        <Detail>
          <StatusIcon />
          State: <p>{state}</p>
        </Detail>
        <DetailLink
          href={user.html_url}
          target='_blank'
          rel='noopener noreferrer'
        >
          <UserIcon />
          GitHub User: <p>{user.login}</p>
        </DetailLink>
        <Detail>
          <IssuesIcon />
          Issue Body:
          <p>{body}</p>
        </Detail>
        <Detail>
          <CommentsIcon />
          Total # of Comments:
          <p>{comments}</p>
        </Detail>
        <Detail>
          <CreatedIcon />
          Created: {createdAt ? createdAt : 'n/a'}
        </Detail>
        <Detail>
          <UpdatedIcon />
          Updated: {updatedAt ? updatedAt : 'n/a'}
        </Detail>
      </Section>
      <Buttons>
        <BackButton
          onClick={() => {
            history.goBack();
          }}
        >
          <BiArrowBack />
          Back to Results
        </BackButton>
      </Buttons>
    </Container>
  );
}

export default withRouter(IssueDetail);
