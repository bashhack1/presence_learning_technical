import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app name', () => {
  render(<App />);
  const appNameElement = screen.getByText(/GitHub Issues Search App/i);
  expect(appNameElement).toBeInTheDocument();
});
