import { ThemeProvider } from 'styled-components';
import {
  Route,
  Switch,
  Redirect,
  BrowserRouter as Router
} from 'react-router-dom';
import Landing from './components/Landing';
import Search from "./components/Search";
import Error from './components/Error';
import IssueDetail from "./components/IssueDetail";
import { GlobalStyle, Theme } from './globals/styles';

function App() {
  return (
    <Router>
      <ThemeProvider theme={Theme}>
        <GlobalStyle />
        <Switch>
          <Route exact path='/error' component={Error} />
          <Route path='/issue/*' component={IssueDetail} />
          <Route path='/search/:owner/:repo' component={Search} />
          <Route exact path='/' component={Landing} />
          <Redirect to='/' />
        </Switch>
      </ThemeProvider>
    </Router>
  );
}

export default App;
