# Presence Learning Technical - Frontend Application

## Overview

I am generally a heavily backend-leaning developer, but have spent some time in the frontend in my career, of course.

Specifically, the 'backend' of the frontend as a JavaScript engineer early in my career.

Given my areas of expertise in backend-development and time away from any rigorous frontend development, I've opted to
go simple and demonstrate wiring up of a frontend with a backend via a simple, small React application.

## Why Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), mostly because it
is a framework I've used in the past - most recently, at my current company, our team is on Ember.js.

I figured by using React, I could prototype something quickly while having ample documentation available despite not
having used the framework directly for a few years - and wow, a lot has changed!

## Structure

I tried to create a simple form, collecting its data and using it to call the project's API server.

On form submission, provided valid GitHub owner and repository names, I've rendered out the response. From there, each
issue rendered can be clicked on for more details.

## Improvements

Probably lots, given my area of expertise isn't frontend development - per se.

That said, a few things I'd wanted to note of are listed below:

ITERATION NOTE:

1. Because GitHub considers pull requests issues (NOTE: look for `pull_request` key on JSON response), we could:
    - create a filter for the frontend
    - add some UI interface to call filtering function (maybe a couple checkboxes)
    - depending on whether checkbox for 'pull request' or 'issue' was selected
    - then show the subset of data results that corresponds to the selection(s)
2. Handle pagination
    - update FastAPI server endpoint to support available query params (per_page + page)
    - create pagination components
    - call server with additional query params
3. The issue body is Markdown (MD) content - could work on some sort of display with formatting feature
    - likely some 3rd-party plugins in React ecosystem to render Markdown
    - leverage those rendering plugins to create rich display in IssueDetail view
4. Proper styling
    - I've managed to throw together some basic styling but obvious room for more specialized UI engineering
5. Testing
    - I kept the default test against `<App />` component and updated, but prioritized backend testing due to time

## Create React App Docs

I've included some key portions of the default Create React App docs below:

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more
information.

#### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Learn More

You can learn more in
the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

#### Code Splitting

This section has moved
here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

#### Analyzing the Bundle Size

This section has moved
here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved
here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

#### Advanced Configuration

This section has moved
here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

#### Deployment

This section has moved
here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

#### `npm run build` fails to minify

This section has moved
here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
