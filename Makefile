.PHONY: fe_up fe_down be_up black isort flake8 lint be_test fe_test

## Run FE via Docker Compose service
fe_up:
	docker compose up frontend

## Shutdown FE via Docker Compose service
fe_down:
	docker compose down

## Run the BE service
be_up:
	cd presence_learning_technical && poetry run uvicorn main:app --reload

## Run a dry-run execution of black
black:
	poetry run black --check presence_learning_technical

## Run a dry-run execution of isort
isort:
	poetry run isort --check-only presence_learning_technical

## Run a dry-run execution of flake8
flake8:
	poetry run flake8 presence_learning_technical

## Convenience task to run all dry-run linting
lint: isort black flake8

## Run pytest against test suite for BE
be_test:
	cd tests && poetry run pytest

## Run jest against test suite for FE
fe_test:
	docker compose exec frontend npm test
