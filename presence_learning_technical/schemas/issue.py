from pydantic import BaseModel


class Issue(BaseModel):
    id: str
    node_id: str
    url: str
    repository_url: str
    labels_url: str
    comments_url: str
    events_url: str
    html_url: str
    number: int
    state: str
    title: str
    body: str
    user: dict
    labels: list[dict]
    assignee: dict | None = None
    assignees: list[dict] | None = None
    milestone: dict | None = None
    locked: bool
    active_lock_reason: str | None = None
    comments: int
    pull_request: dict | None = None
    closed_at: str | None = None
    created_at: str
    updated_at: str
    closed_by: dict | None = None
    author_association: str
    site_admin: bool | None = None
