from functools import lru_cache
from typing import Annotated

from fastapi import APIRouter, Depends

from presence_learning_technical.core.config import Settings

router = APIRouter()


@lru_cache()
def get_settings():
    """Least Recently Used cache to prevent having to re-load settings per request"""
    return Settings()


@router.get("/")
async def info(app_settings: Annotated[Settings, Depends(get_settings)]):
    """Returns application-level settings"""
    return {
        "project_name": app_settings.PROJECT_NAME,
        "github_token": app_settings.GITHUB_TOKEN,
    }
