from typing import Annotated

from fastapi import APIRouter, Path, Query

from presence_learning_technical.schemas.issue import Issue
from presence_learning_technical.services.github import gh_get_all_issues, gh_get_issue

router = APIRouter()

error_response_for_all = {
    404: {
        "description": "Error: Not Found",
        "content": {
            "application/json": {
                "example": {
                    "detail": "Oops! No resources found for Owner: XXX, Repo: XXX"
                }
            }
        },
    }
}

error_response_for_single = {
    404: {
        "description": "Error: Not Found",
        "content": {
            "application/json": {
                "example": {
                    "detail": "Oops! No resources found for Issue ID: XXX, Owner: XXX, Repo: XXX"
                }
            }
        },
    }
}


@router.get("/", response_model=list[Issue], responses={**error_response_for_all})
async def get_all_issues(
    owner: Annotated[str, Query(title="The owner of the GitHub repository")],
    repo: Annotated[str, Query(title="The name of the GitHub repository")],
):
    """Returns all issues for a GitHub repository, given an owner + repo"""
    response = await gh_get_all_issues(owner, repo)
    return response


@router.get(
    "/{issue_id}", response_model=Issue, responses={**error_response_for_single}
)
async def get_issue(
    issue_id: Annotated[
        int, Path(title="The ID of the issue within the GitHub repository")
    ],
    owner: Annotated[str, Query(title="The owner of the GitHub repository")],
    repo: Annotated[str, Query(title="The name of the GitHub repository")],
):
    """Returns an issue for a GitHub repository, given an owner + repo"""
    response = await gh_get_issue(issue_id, owner, repo)
    return response
