from fastapi import APIRouter

from presence_learning_technical.api.api_v1.endpoints import application_info, issues

api_router = APIRouter()
api_router.include_router(
    application_info.router, prefix="/application_info", tags=["application_info"]
)
api_router.include_router(issues.router, prefix="/issues", tags=["issues"])
