import httpx
from fastapi import HTTPException, status

from presence_learning_technical.core.config import settings

# ITERATION NOTE:
# 1. Create custom exception messaging for known additional responses for GET single issue:
#    - 301 (moved permanently) // 304 (not modified) // 410 (gone)
# 2. Create custom exception messaging for known additional responses for GET all issues:
#    - 301 (moved permanently) // 422 (validation failed, or endpoint spammed)


GITHUB_ROOT_URL = "https://api.github.com/repos"

headers = {
    "Accept": "application/vnd.github+json",
    "Authorization": f"Bearer {settings.GITHUB_TOKEN or 'FAKE_TOKEN'}",
    "X-GitHub-Api-Version": "2022-11-28",
}


async def gh_get_all_issues(owner: str, repo: str):
    async with httpx.AsyncClient() as client:
        url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues"
        response = await client.get(url, headers=headers)
        if response.status_code == status.HTTP_404_NOT_FOUND:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Oops! No resources found for Owner: {owner}, Repo: {repo}",
            )
        return response.json()


async def gh_get_issue(issue_id: int, owner: str, repo: str):
    async with httpx.AsyncClient() as client:
        url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues/{issue_id}"

        # ITERATION NOTE:
        # Assuming GitHub API supports caching headers, then implement caching layer logic:
        #  - If fetching for same resource (CHECK ALL params... + GITHUB TOKEN)
        #  - Then check if ETag + Last-Modified present in cache
        #  - Then submit GET with HTTP caching headers
        #  - Finally, handle 304
        #
        # Ex. headers = {**headers, "If-Modified-Since": ..., "If-None-Match": ...}

        response = await client.get(url, headers=headers)
        if response.status_code == status.HTTP_404_NOT_FOUND:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=(
                    "Oops! No resource found for "
                    f"Issue ID: {issue_id}, Owner: {owner}, Repo: {repo}"
                ),
            )
        return response.json()
