from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from presence_learning_technical.api.api_v1.api import api_router
from presence_learning_technical.core.config import settings

description = """
Presence Technical Interview API gives you the power to see issues! 🚀

## Application Info

You can **read app info**.

## Issues

You will be able to:

* **GET all issues** for a GitHub repository
* **GET a single issue** for a GitHub repository
* **POST a single issue** for a GitHub repository (not implemented)
* **PATCH a single issue** for a GitHub repository (not implemented)
"""

tags_metadata = [
    {
        "name": "application_info",
        "description": "Overview of application settings.",
    },
    {
        "name": "issues",
        "description": "Reading, writing, and updating Github issues.",
        "externalDocs": {
            "description": "GitHub Official Docs (Issues)",
            "url": (
                "https://docs.github.com/en/rest/issues/issues?apiVersion=2022-11-28"
            ),
        },
    },
]

app = FastAPI(
    title=settings.PROJECT_NAME,
    description=description,
    version="0.0.1",
    openapi_tags=tags_metadata,
    openapi_url=f"{settings.API_V1_STR}/openapi.json",
)

origins = [
    # Set CORS to allow for React app to hit API
    "http://localhost:3000",
    "localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(api_router, prefix=settings.API_V1_STR)
