# Presence Learning - Technical Exercise

## Overview

This project is a full-stack application that includes:

- an API server that fetches data from GitHub about repository issues
- a frontend application to render the results of calling the server

## Pre-Requisites

ITERATION NOTE: I typically bundle up my Python applications in a Docker image, composing them via
a `docker-compose.yml`.
For the sake of this exercise, I've Docker-ized the frontend, but given more time would also do the same for the FastAPI
application.

Running this project requires the following dependencies:

* Make
* Docker (this README assumes this is installed via Docker Desktop for Mac or similar)
* Python 3.10+

### Python Installation

0.0 Install Pyenv (Python runtime isolation)

```shell
brew update
brew install pyenv
```

0.1 Set Pyenv configuration

For *Zsh*:

```shell
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
```

For *bash*:

```shell
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
```

0.2 Install Python 3.10.11

```shell
# From root of project
pyenv install 3.10.11
```

0.3 Set the local Python context and confirm

```shell
pyenv local 3.10.11
which python
python
```

If everything is installed correctly, you should see the following output, respectively:

```shell
/Users/username/.pyenv/shims/python
```

```shell
Python 3.10.11 (main, Apr 11 2023, 00:36:48) [Clang 14.0.3 (clang-1403.0.22.14.1)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

### Poetry Installation

1. Install Poetry and confirm

```shell
curl -sSL https://install.python-poetry.org | python3 -
poetry --version
```

2. Configure creation of virtualenvs within project

```shell
poetry config virtualenvs.in-project true
```

### ENV File Configuration

Next, copy the `.env.example` file to a root-dir `.env`, like so:

```shell
cp .env.example .env
```

Once completed, update the new `.env` file filling out:

* `PROJECT_NAME=`
* `GITHUB_TOKEN=`

(For instructions on finding your GitHub auth token, see `.env` file!)

## Running the project

Before proceeding, ensure all Python dependencies have been installed via Poetry:

```shell
poetry install
```

Next, execute the following:

1. In one terminal, run `make be_up`
2. In another terminal, run `make fe_up`

**Optional**

You may wish to run the following additional `Make` tasks:

* `make fe_down` to shutdown the frontend service
* `make lint` to run `black`, `isort` and `flake8` static analysis
* `make be_test` to execute `pytest` against the backend test suite
* `make fe_test` to execute `jest` against the frontend test suite
