import pytest
import respx

from fastapi import HTTPException

from presence_learning_technical.services import github
from presence_learning_technical.services.github import GITHUB_ROOT_URL

mock_issue = {
    'id': '1809922064000',
    'node_id': 'PR_00DO0AJ0cs50dBb0',
    'url': 'https://api.github.com/repos/open/source/issues/9012171',
    'repository_url': 'https://api.github.com/repos/open/source',
    'labels_url': 'https://api.github.com/repos/open/source/issues/9012171/labels{/name}',
    'comments_url': 'https://api.github.com/repos/open/source/issues/9012171/comments',
    'events_url': 'https://api.github.com/repos/open/source/issues/9012171/events',
    'html_url': 'https://github.com/open/source/pull/9012171',
    'number': 9012171,
    'state': 'open',
    'title': "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    'body': "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore",
    'user': {
        'login': 'pariatur_excepteur',
        'id': 90216106000,
        'node_id': '00M0Q6V0Nl0jEx00E0N0A=',
        'avatar_url': 'https://avatars.githubusercontent.com/u/0216106000?v=4',
        'gravatar_id': '',
        'url': 'https://api.github.com/users/pariatur_excepteur',
        'html_url': 'https://github.com/pariatur_excepteur',
        'followers_url': 'https://api.github.com/users/pariatur_excepteur/followers',
        'following_url': 'https://api.github.com/users/pariatur_excepteur/following{/other_user}',
        'gists_url': 'https://api.github.com/users/pariatur_excepteur/gists{/gist_id}',
        'starred_url': 'https://api.github.com/users/pariatur_excepteur/starred{/owner}{/repo}',
        'subscriptions_url': 'https://api.github.com/users/pariatur_excepteur/subscriptions',
        'organizations_url': 'https://api.github.com/users/pariatur_excepteur/orgs',
        'repos_url': 'https://api.github.com/users/pariatur_excepteur/repos',
        'events_url': 'https://api.github.com/users/pariatur_excepteur/events{/privacy}',
        'received_events_url': 'https://api.github.com/users/pariatur_excepteur/received_events',
        'type': 'User',
        'site_admin': False
    },
    'labels': [],
    'assignee': None,
    'assignees': [],
    'milestone': None,
    'locked': False,
    'active_lock_reason': None,
    'comments': 0,
    'pull_request': {
        'url': 'https://api.github.com/repos/open/source/pulls/9012171',
        'html_url': 'https://github.com/open/source/pull/9012171',
        'diff_url': 'https://github.com/open/source/pull/9012171.diff',
        'patch_url': 'https://github.com/open/source/pull/9012171.patch',
        'merged_at': None
    },
    'closed_at': None,
    'created_at': '2023-05-14T12:20:07Z',
    'updated_at': '2023-05-14T13:15:57Z',
    'closed_by': None,
    'author_association': 'CONTRIBUTOR',
    'site_admin': None
}


@respx.mock
@pytest.mark.anyio
async def test_get_issue_mocked() -> None:
    issue_id = 1
    owner = repo = "fakeinput"
    url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues/{issue_id}"
    endpoint = respx.get(url).respond(json=mock_issue)
    result = await github.gh_get_issue(issue_id=issue_id, owner=owner, repo=repo)
    assert endpoint.called
    assert result == mock_issue


@respx.mock
@pytest.mark.anyio
async def test_get_issue_404_mocked() -> None:
    issue_id = 1
    owner = repo = "fakeinput"
    url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues/{issue_id}"
    endpoint = respx.get(url).respond(404)
    with pytest.raises(HTTPException) as exc:
        _ = await github.gh_get_issue(issue_id=issue_id, owner=owner, repo=repo)

    assert endpoint.called
    assert f"Oops! No resource found for Issue ID: {issue_id}, Owner: {owner}, Repo: {repo}" in str(exc)


@respx.mock
@pytest.mark.anyio
async def test_get_all_issues_mocked() -> None:
    owner = repo = "fakeinput"
    url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues"
    endpoint = respx.get(url).respond(json=[mock_issue])
    result = await github.gh_get_all_issues(owner=owner, repo=repo)
    assert endpoint.called
    assert result == [mock_issue]


@respx.mock
@pytest.mark.anyio
async def test_get_all_issues_404_mocked() -> None:
    owner = repo = "fakeinput"
    url = f"{GITHUB_ROOT_URL}/{owner}/{repo}/issues"
    endpoint = respx.get(url).respond(404)
    with pytest.raises(HTTPException) as exc:
        _ = await github.gh_get_all_issues(owner=owner, repo=repo)

    assert endpoint.called
    assert f"Oops! No resources found for Owner: {owner}, Repo: {repo}" in str(exc)
