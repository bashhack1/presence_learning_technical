from fastapi.testclient import TestClient

from presence_learning_technical.core.config import settings
from presence_learning_technical.schemas.issue import Issue


def test_get_all_issues(
    client: TestClient
) -> None:
    r = client.get(f"{settings.API_V1_STR}/issues?owner=redis&repo=redis")
    all_issues = r.json()

    issue = all_issues[0]

    assert type(all_issues) == list
    assert len(all_issues) > 0
    assert issue.keys() == Issue.schema()["properties"].keys()


def test_get_all_issues_not_found(client: TestClient) -> None:
    owner = "not"
    repo = "real"
    r = client.get(f"{settings.API_V1_STR}/issues?owner={owner}&repo={repo}")
    response = r.json()

    assert response["detail"] == f"Oops! No resources found for Owner: {owner}, Repo: {repo}"


def test_get_issue(client: TestClient) -> None:
    r = client.get(f"{settings.API_V1_STR}/issues?owner=redis&repo=redis")
    all_issues = r.json()

    first_issue = all_issues[0]
    issue_id = first_issue["number"]

    r = client.get(f"{settings.API_V1_STR}/issues/{issue_id}?owner=redis&repo=redis")
    issue = r.json()

    assert type(issue) == dict
    assert issue.keys() == Issue.schema()["properties"].keys()


def test_get_issue_not_found(client: TestClient) -> None:
    issue_id = 9999
    owner = "not"
    repo = "real"
    r = client.get(f"{settings.API_V1_STR}/issues/{issue_id}/?owner={owner}&repo={repo}")
    response = r.json()

    assert response["detail"] == f"Oops! No resource found for Issue ID: {issue_id}, Owner: {owner}, Repo: {repo}"
