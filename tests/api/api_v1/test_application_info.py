from fastapi.testclient import TestClient

from presence_learning_technical.api.api_v1.endpoints.application_info import get_settings
from presence_learning_technical.core.config import settings, Settings
from presence_learning_technical.main import app


def get_settings_override():
    return Settings(PROJECT_NAME="Test Project")


app.dependency_overrides[get_settings] = get_settings_override


def test_get_application_info(
    client: TestClient
) -> None:
    r = client.get(f"{settings.API_V1_STR}/application_info")
    application_info = r.json()
    assert application_info["project_name"] == "Test Project"
